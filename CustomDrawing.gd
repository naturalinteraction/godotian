extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var rotation_angle = 50
var angle_from = 75
var angle_to = 195


# Called when the node enters the scene tree for the first time.
func _ready():
	print('custom drawing ready')
	pass # Replace with function body.

func draw_circle_arc(center, radius, from, to, color):
	var nb_points = 64
	var points_arc = PoolVector2Array()

	for i in range(nb_points + 1):
		var angle_point = deg2rad(from + i * (to-from) / nb_points - 90)
		points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

	for index_point in range(nb_points):
		draw_line(points_arc[index_point], points_arc[index_point + 1], color, 3.0, true)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	angle_from += rotation_angle * delta
	angle_to += rotation_angle * delta
	# We only wrap angles when both of them are bigger than 360.
	if angle_from > 360 and angle_to > 360:
		angle_from = wrapf(angle_from, 0, 360)
		angle_to = wrapf(angle_to, 0, 360)
	# print(delta)
	update()
#	pass

func _draw():
   var center = Vector2(0, 0)
   #var radius = 180
   #var color = Color(1.0, 0.0, 0.0)
   var color2 = Color(0.0, 0.0, 1.0)

   #draw_circle_arc( center, radius, angle_from, angle_to, color )
   for i in range(100):
	   #draw_arc( center, radius * (1.0 + 0.1 * i), angle_from * 0.0174533, angle_to * 0.0174533, 64, color2, 3.0, true)
	   draw_line(center, Vector2(angle_to, angle_from * (1.0 + 0.1 * i)), color2, 3.0, false)


